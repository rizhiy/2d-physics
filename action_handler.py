import numpy as np
from easydict import EasyDict

from window_manager import CloseWindowException, WindowManager


def handle_key(key, wm: WindowManager, cfg: EasyDict):
    action = cfg.KEY_MAPPING.get(str(key), '')
    if action == 'QUIT':
        raise CloseWindowException()
    if action == 'ZOOM_OUT':
        wm.zoom *= 0.99
    if action == 'ZOOM_IN':
        wm.zoom *= 1.01

    if action == 'MOVE_LEFT':
        wm.move(np.array((-0.1, 0)) * wm.zoom)
    if action == 'MOVE_RIGHT':
        wm.move(np.array((0.1, 0)) * wm.zoom)
    if action == 'MOVE_UP':
        wm.move(np.array((0, -0.1)) * wm.zoom)
    if action == 'MOVE_DOWN':
        wm.move(np.array((0, 0.1)) * wm.zoom)

    if action == 'CENTRE_VIEW':
        centre = np.array((0, 0), dtype=float)
        total_mass = 0
        for body in wm.bodies:
            centre += body.position * body.mass
            total_mass += body.mass
        centre /= total_mass

        wm._position = wm._position * 0.95 + centre * 0.05
