from typing import Tuple

import cv2
import numpy as np

from bodies import Body


class CloseWindowException(Exception):
    pass


class WindowManager:
    def __init__(self, size: Tuple[int, int], background=(255, 255, 255), title='Window'):
        self._size = np.array(size, dtype=int)
        self._background = np.array(background, dtype=np.uint8)
        self._title = title
        self._position = np.array((0, 0), dtype=float)
        self.zoom = 1
        self.buffer = None
        self.clear_buffer()
        self._bodies = []

    def __enter__(self):
        self.show_frame()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        cv2.destroyAllWindows()
        if exc_type is CloseWindowException:
            return True

    def show_frame(self):
        cv2.imshow(self._title, self.buffer)
        return cv2.waitKey(1) & 0xFF

    def clear_buffer(self):
        self.buffer = np.tile(self._background, (*self._size, 1))

    @property
    def position(self):
        return self._position

    def calculate_coords(self, coords):
        return (coords - self.position) * self.zoom + self._size[::-1] / 2

    def move(self, diff: np.ndarray):
        self._position += diff

    def add_body(self, body: Body):
        self._bodies.append(body)

    def draw(self):
        for body in self._bodies:
            body.draw(self)

    @property
    def bodies(self):
        return self._bodies
