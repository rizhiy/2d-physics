from itertools import count
from pathlib import Path

import cv2
import numpy as np
from tqdm import tqdm

from action_handler import handle_key
from bodies import Circle
from config import load_cfg
from window_manager import WindowManager

size = (1080, 1920)
key_mapping = {}

background = np.array((255, 255, 255), dtype=np.uint8)

bodies = [Circle(25, 5, position=np.array((0, 0), dtype=float)),
          Circle(1, 1, position=np.array((10, 0), dtype=float), velocity=np.array((0, 0.5), dtype=float)),
          Circle(4, 2, position=np.array((0, 40), dtype=float), velocity=np.array((-0.27, 0), dtype=float))]

bodies[0].colour = (255, 0, 0)
bodies[1].colour = (0, 0, 255)

cfg = load_cfg(Path('configs/base.yaml'))

np.set_printoptions(precision=3)
with WindowManager(size) as wm:
    wm.zoom = 10
    for body in bodies:
        wm.add_body(body)
    for frame_idx in tqdm(count(), disable=True):
        if frame_idx % 100 == 0:
            for body_idx, body in enumerate(bodies):
                print(frame_idx, body_idx, body.position)
        # What if gravity constant was a random variable
        # cfg.G = np.random.uniform(cfg._G * 0.99, cfg._G / 0.99)
        cfg.G = cfg._G
        wm.clear_buffer()
        for body in bodies:
            total_force = np.array((0, 0), dtype=float)
            for another in bodies:
                if another is body:
                    continue
                distance = body.distance_to_another(another)
                min_distance = body.max_size + another.max_size
                # collision
                if distance < min_distance:
                    raise NotImplementedError("Collision!")
                vector = body.normal_vector_to_another(another)
                force = body.gravity_force_to_another(another, cfg)
                total_force += vector * force
            body.acceleration = total_force / body.mass

        for body in bodies:
            body.update_position()

        wm.draw()
        cv2.putText(wm.buffer, f"{frame_idx:d}", (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
        key = wm.show_frame()
        handle_key(key, wm, cfg)
