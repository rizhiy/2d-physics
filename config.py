from copy import copy
from pathlib import Path

import yaml
from easydict import EasyDict

_cfg = EasyDict()

# Interface
_cfg.SIZE = (1080, 1920)
_cfg.KEY_MAPPING = EasyDict()
_cfg.KEY_MAPPING['27'] = 'QUIT'
_cfg.KEY_MAPPING[str(ord('+'))] = 'ZOOM_IN'
_cfg.KEY_MAPPING[str(ord('-'))] = 'ZOOM_OUT'
_cfg.KEY_MAPPING['81'] = 'MOVE_LEFT'
_cfg.KEY_MAPPING['82'] = 'MOVE_UP'
_cfg.KEY_MAPPING['83'] = 'MOVE_RIGHT'
_cfg.KEY_MAPPING['84'] = 'MOVE_DOWN'
_cfg.KEY_MAPPING[str(ord('c'))] = 'CENTRE_VIEW'

# Physics config
_cfg._G = 1


def _merge_cfg(new_cfg: dict, base_cfg: EasyDict) -> EasyDict:
    cfg = copy(base_cfg)
    for key, value in new_cfg.items():
        if isinstance(value, dict):
            cfg[key] = _merge_cfg(value, cfg[key])
        else:
            cfg[key] = value
    return cfg


def load_cfg(config_path: Path) -> EasyDict:
    with config_path.open() as f:
        return _merge_cfg(yaml.load(f), _cfg)
