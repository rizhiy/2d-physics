from __future__ import annotations

import math

import cv2
import numpy as np
from easydict import EasyDict


class Body:
    def __init__(self, mass: int, position=np.array([0, 0], dtype=float), velocity=np.array([0, 0], dtype=float)):
        self._mass = mass
        self._position = position
        self._velocity = velocity
        self.new_position = None
        self.new_velocity = None
        self.acceleration = np.array([0, 0], dtype=float)
        self.elasticity = 0.95
        self.colour = (0, 255, 0)

    def distance_to_another(self, another: 'Body'):
        return math.sqrt(
            (self._position[0] - another._position[0]) ** 2 + (self._position[1] - another._position[1]) ** 2)

    def draw(self, wm: WindowManager):
        raise NotImplementedError

    def move(self, diff: np.ndarray):
        self._position += diff

    def gravity_force_to_another(self, another: 'Body', cfg: EasyDict):
        distance = self.distance_to_another(another)
        return cfg.G * self._mass * another._mass / distance ** 2

    def normal_vector_to_another(self, another: 'Body'):
        distance = self.distance_to_another(another)
        return (another._position - self._position) / distance

    def update_position(self):
        if self.new_velocity is not None:
            self._velocity = self.new_velocity
            self.new_velocity = None
            self.acceleration *= 0.25
        self._velocity += self.acceleration

        if self.new_position is not None:
            self._position = self.new_position
            self.new_position = None
            self._velocity *= self.elasticity
        self._position += self._velocity

    @property
    def max_size(self):
        raise NotImplementedError

    @property
    def mass(self):
        return self._mass

    @property
    def position(self):
        return self._position

    @property
    def velocity(self):
        return self._velocity


class Circle(Body):
    def __init__(self, mass: int, radius: float, **kwargs):
        super(Circle, self).__init__(mass, **kwargs)
        self._radius = radius

    def draw(self, wm: WindowManager):
        window_coords = tuple(map(int, wm.calculate_coords(self._position)))
        window_radius = int(self._radius * wm.zoom)
        cv2.circle(wm.buffer, window_coords, window_radius, self.colour, 1)

    @property
    def max_size(self):
        return self._radius
